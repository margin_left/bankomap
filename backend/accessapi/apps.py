from django.apps import AppConfig


class AccessapiConfig(AppConfig):
    name = 'accessapi'
