from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
# router.register(r'geocode', views.CoordsViewSet, basename='geocoder')
router.register(r'sbers', views.SbersViewSet, basename='bankomats')
router.register(r'streets', views.StreetsViewSet, basename='streets')


urlpatterns = router.urls

urlpatterns.extend([
    path(r'navigate/', views.NavigateViewSet.as_view(), name='navigate')
])
