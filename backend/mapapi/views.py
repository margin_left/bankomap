# -*- coding: UTF-8 -*-

import json
from pathlib import Path

from django.db import connection
from django.http import JsonResponse
from django.views import View
from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import Sbers, Streets, StreetSerializer, CoordsSerializer, SberSerializer
from .objects_detection import cvision_picture


class SbersViewSet(viewsets.ModelViewSet):
    queryset = Sbers.objects.all()
    serializer_class = SberSerializer


class StreetsViewSet(viewsets.ViewSet):
    queryset = Streets.objects.all()
    serializer_class = StreetSerializer


class NavigateViewSet(View):

    def get(self, request, *args, **kwargs):
        navigation = request.GET.get('navigation')
        lon_lat = navigation.split(', ')
        with connection.cursor() as cursor:
            cursor.execute(f'''
                select 
                    geom, 
                    st_distance(
                        ST_Transform(geom, 3857), 
                        ST_Transform('srid=4326;POINT({str(lon_lat[1])} {str(lon_lat[0])})', 3857)
                    ) 
                from tableapi.sbers
            ''')

            rows = [row for row in cursor.fetchall()]

            min_dist = 99999999
            for row in rows:
                if row[1] < min_dist:
                    min_dist = row[1]

            cursor.execute(f'''
                Select 
                    st_asgeojson(
                        ST_Intersection(
                            ST_Transform(geom, 4326),
                            ST_Buffer(
                                ST_Transform('srid=4326;POINT({str(lon_lat[1])} {str(lon_lat[0])})', 4326),
                                {2 * min_dist}
                            )
                        )
                    )
                FROM  tableapi.test_line
            ''')

            rows = [row for row in cursor.fetchall()]

            return JsonResponse(json.dumps(rows), safe=False)


def check_the_picture(index: int = 1):
    parent = Path('..')
    path_to_image = parent / 'test_pictures' / f'peo{index}.jpg'
    print(path_to_image)
    res = cvision_picture(image_picture=path_to_image)
    print(res)
