from rest_framework import serializers

from .models import Sbers, Streets


class SberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sbers
        fields = '__all__'


class StreetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Streets
        fields = '__all__'


class CoordsSerializer:
    pass
