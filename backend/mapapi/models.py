from django.contrib.gis.db import models


class Sbers(models.Model):
    geom = models.PointField()


class Streets(models.Model):
    name = models.CharField(max_length=100)
    geom = models.LineStringField()
