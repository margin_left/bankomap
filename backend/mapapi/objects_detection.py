import argparse
import cv2
import imutils
import numpy as np
from pathlib import Path
import time

# from .nc import train

# initialize class labels to detect
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]
# generate a set of bounding box colors for each class
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))
# Segnet path
BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent

# ap = argparse.ArgumentParser()
# ap.add_argument("-p", "--prototxt", required=True,
#                 help="path to Caffe 'deploy' prototxt file")
# ap.add_argument("-m", "--model", required=True,
#                 help="path to Caffe pre-trained model")
# ap.add_argument("-c", "--confidence", type=float, default=0.2,
#                 help="minimum probability to filter weak detections")
# args = vars(ap.parse_args())


def cvision_picture(blob_picture=None, image_picture=None):
    # load serialized model
    # net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
    prototxt_filename = BASE_DIR / 'segnet' / 'caffe-segnet' / 'models' / 'bvlc_googlenet' / 'quick_solver.prototxt'
    print(prototxt_filename)
    # caffemodel = train(prototxt_filename)
    net = cv2.dnn.readNetFromCaffe(prototxt_filename, "bvlc_googlenet.caffemodel")
    image = blob_picture
    if image_picture:
        # преобразование изображения в blob с модулем dnn
        image = cv2.dnn.blobFromImage(image_picture, size=(299, 299))

    # pass the blob through the network
    # устанавливаем blob как входные данные в нейросеть
    net.setInput(image)
    # передаём эти данные через net, чтобы обнаружить предметы
    detections = net.forward()
    # rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # loop over the detections
    for i in np.arange(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections by ensuring the `confidence` is greater than the minimum confidence
        if confidence > args["confidence"]:
            # extract the index of the class label from the
            # `detections`, then compute the (x, y)-coordinates of
            # the bounding box for the object
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # draw the prediction on the frame
            label = "{}: {:.2f}%".format(CLASSES[idx],
                                         confidence * 100)
            cv2.rectangle(image, (startX, startY), (endX, endY),
                          COLORS[idx], 2)
            y = startY - 15 if startY - 15 > 15 else startY + 15
            cv2.putText(image, label, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)
