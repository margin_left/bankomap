from django.shortcuts import render
from django.views import View


class IndexView(View):
    template_name = 'project/index.html'

    def get(self, request, **kwargs):
        context = dict()
        response = render(request, self.template_name, context)
        return response
