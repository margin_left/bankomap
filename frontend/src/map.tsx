import * as ol from "ol";
import { View } from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";

import olVectorLayer from "ol/layer/Vector";
import olVectorSource from "ol/source/Vector";
import olFeature from "ol/Feature";
import olLineString from "ol/geom/LineString";

import { get } from "ol/proj";

export const graphLayer = new olVectorLayer({
  source: new olVectorSource({
    features: [
      new olFeature({
        geometry: new olLineString([
          [4420705.67, 5980276.64],
          [4421935.83, 5980326.8],
          [4421028.14, 5979440.61],
          [4422650.04, 5979450.16],
          [4421716.07, 5978721.62],
        ]),
      }),
      new olFeature({
        geometry: new olLineString([
          [4421281.34, 5980491.62],
          [4421090.25, 5979994.77],
          [4422050.49, 5979775.02],
          [4421814.01, 5978974.82],
        ]),
      }),
    ],
  }),
});

export const initialMap = () => {
  const currentProjection = get("EPSG:3857");

  const map = new ol.Map({
    target: "map",
    layers: [
      new TileLayer({
        source: new OSM(),
      }),
      graphLayer,
    ],
    view: new View({
      projection: currentProjection,
      center: [4421281.34, 5980491.62],
      zoom: 15,
    }),
  });

  return map;
};
