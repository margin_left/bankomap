import * as ol from "ol";
import { Map, View } from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";

import olVectorLayer from "ol/layer/Vector";
import olVectorSource from "ol/source/Vector";
import olFeature from "ol/Feature";
import olLineString from "ol/geom/LineString";

import olPointGeom from "ol/geom/Point";

import { joinGraph, getGraphs, searchTrack } from "./graph-utils";

import olStyle from "ol/style/Style";
import olFillStyle from "ol/style/Fill";
import olStrokeStyle from "ol/style/Stroke";
import olTextStyle from "ol/style/Text";

import { initialMap, graphLayer } from "./map";

const map = initialMap();

const d = document.getElementById("some-action");
d.onclick = () => {
  const graphs = getGraphs(map, graphLayer.getSource().getFeatures());
  const currentGraph = joinGraph(graphs[0], graphs[1]);

  const pointLayer = new olVectorLayer({
    source: new olVectorSource({
      features: [
        new olFeature({
          geometry: new olPointGeom(
            map.getCoordinateFromPixel(currentGraph[5][0])
          ),
        }),
        new olFeature({
          geometry: new olPointGeom(
            map.getCoordinateFromPixel(currentGraph[10][0])
          ),
        }),
      ],
    }),
  });
  map.addLayer(pointLayer);

  const st = searchTrack(
    currentGraph[5][0],
    currentGraph[10][0],
    currentGraph
  )[0];
  const trackLayer = new olVectorLayer({
    style: (feature: olFeature) => {
      const dimensionLineGeom = feature.getGeometry();

      if (!(dimensionLineGeom instanceof olLineString)) {
        throw new Error("Геометрия не имеет тип LineString");
      }

      const [[x1, y1], [x2, y2]] = dimensionLineGeom.getCoordinates();

      const dx = x1 - x2;
      const dy = y1 - y2;
      const text = Math.sqrt(dx * dx + dy * dy).toFixed(1);

      return new olStyle({
        stroke: new olStrokeStyle({ color: "#000000" }),
        fill: new olFillStyle({ color: "#000000" }),
        text: new olTextStyle({
          placement: "line",
          stroke: new olStrokeStyle({ color: "white", width: 3 }),
          fill: new olFillStyle({ color: "#000000" }),
          text: String(text),
          font: "14px sans-serif",
          textAlign: "center",
          rotateWithView: true,
          textBaseline: "bottom",
          overflow: true,
        }),
      });
    },
    source: new olVectorSource({
      features: [
        new olFeature({
          geometry: new olLineString(
            st.map((coord) => map.getCoordinateFromPixel(coord))
          ),
        }),
      ],
    }),
  });

  map.addLayer(trackLayer);
};

/*
s - точка геолокации
i - точка назначения
А [i][j] - матрица длин кратчайшего пути из s в i
**/
