import { Map } from "ol";
import olFeature from "ol/Feature";
import { point, graph, getPixelCoord, getIntersects } from "./geom-utils";

export const joinGraph = (g1: graph, g2: graph): graph => {
  const intersects = getIntersects(g1, g2);
  const resGraph: graph = [];

  if (intersects.length === 0) {
    return resGraph;
  }

  let startSliceG1 = 0;
  let startSliceG2 = 0;
  for (let int of intersects) {
    resGraph.push(...g1.slice(startSliceG1, int.edge1));
    resGraph.push(...g2.slice(startSliceG2, int.edge2));

    resGraph.push([g1[int.edge1][0], int.intersect]);
    resGraph.push([int.intersect, g1[int.edge1][1]]);

    resGraph.push([g2[int.edge2][0], int.intersect]);
    resGraph.push([int.intersect, g2[int.edge2][1]]);

    startSliceG1 = int.edge1 + 1;
    startSliceG2 = int.edge2 + 1;
  }

  if (startSliceG1 < g1.length) {
    resGraph.push(...g1.slice(startSliceG1, g1.length));
  }
  if (startSliceG2 < g2.length) {
    resGraph.push(...g2.slice(startSliceG2, g2.length));
  }

  return resGraph;
};

export const getGraphs = (map: Map, features: olFeature[]) => {
  const pixelCoords = features.map((feature) =>
    getPixelCoord(map, feature.getGeometry())
  );

  const graphs: graph[] = [];

  pixelCoords.forEach((pixelCoord) => {
    const graphNodes: graph = [];
    for (let i = 0; i < pixelCoord.length - 1; i++) {
      graphNodes.push([pixelCoord[i], pixelCoord[i + 1]]);
    }
    graphs.push(graphNodes);
  });

  return graphs;
};

export const pointEqual = (p1: point, p2: point) => {
  return p1[0] === p2[0] && p1[1] === p2[1];
};

export const getNextPoint = (
  p: point,
  edges: graph,
  badPoints: point[] = []
): point[] => {
  const resPoint = [];
  for (let edge of edges) {
    if (
      pointEqual(p, edge[0]) &&
      !badPoints.filter((point) => pointEqual(point, edge[1])).length
    ) {
      resPoint.push(edge[1]);
    }
    if (
      pointEqual(p, edge[1]) &&
      !badPoints.filter((point) => pointEqual(point, edge[0])).length
    ) {
      resPoint.push(edge[0]);
    }
  }

  return resPoint;
};

export const searchTrack = (
  pBegin: point,
  pEnd: point,
  edges: graph,
  badPoints: point[] = []
): point[][] | undefined => {
  const newBadPoints = badPoints.concat([pBegin]);
  const nextPoints = getNextPoint(pBegin, edges, badPoints);

  if (nextPoints.length === 0) {
    return;
  }

  const dists: point[][] = [];
  for (let nextPoint of nextPoints) {
    if (pointEqual(nextPoint, pEnd)) {
      dists.push([pBegin, nextPoint]);
    } else {
      let nDists = searchTrack(
        nextPoint,
        pEnd,
        edges,
        newBadPoints.concat([nextPoint])
      );

      if (nDists !== undefined) {
        nDists = nDists.map((d) => {
          let newD = [...d];
          newD.unshift(pBegin);

          return newD;
        });

        dists.push(...nDists);
      }
    }
  }

  return dists;
};
