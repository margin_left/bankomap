import * as ol from "ol";
import { Map, View } from "ol";
import TileLayer from "ol/layer/Tile";
import OSM from "ol/source/OSM";

import olVectorLayer from "ol/layer/Vector";
import olVectorSource from "ol/source/Vector";
import olFeature from "ol/Feature";
import olLineString from "ol/geom/LineString";

import olGeometries from "ol/geom/Geometry";
import olPointGeom from "ol/geom/Point";

import { get } from "ol/proj";

import olStyle from "ol/style/Style";
import olFillStyle from "ol/style/Fill";
import olStrokeStyle from "ol/style/Stroke";
import olCircleStyle from "ol/style/Circle";
import olTextStyle from "ol/style/Text";

export type point = [number, number];
export type graph = [point, point][];

/** Возвращает коэффициенты общего уравнения прямой */
export function getGeneralFactor(line: [point, point]) {
  const [x1, y1] = line[0];
  const [x2, y2] = line[1];

  if (x1 === x2 && y1 === y2) {
    throw new Error(
      "Невозможно построить уравнение прямой, так как точки прямой имеют одинаковые координаты"
    );
  }

  const A = y2 - y1;
  const B = -(x2 - x1);
  const C = -x1 * y2 + x1 * y1 + x2 * y1 - x1 * y1;

  return { A, B, C };
}
``;
/** Возвращает угловые коэффициенты уравнения прямой */
export function getAngularFactor(line: [point, point]) {
  const { A, B, C } = getGeneralFactor(line);

  if (B === 0) {
    throw new Error(
      "Невозможно построить уравнение прямой, так как прямая совпадает с осью Ox"
    );
  }

  const k = -A / B;
  const b = -C / B;

  return { k, b };
}

/** Возвращает точку пересечения */
export function intersectPoint(
  line1: [point, point],
  line2: [point, point]
): point {
  if (line1[0][0] === line1[1][0]) {
    const { k, b } = getAngularFactor(line2);

    return [line1[0][0], k * line1[0][0] + b];
  }

  if (line2[0][0] === line2[1][0]) {
    const { k, b } = getAngularFactor(line1);

    return [line2[0][0], k * line2[0][0] + b];
  }

  const fact1 = getAngularFactor(line1);
  const fact2 = getAngularFactor(line2);

  const a = fact1.k;
  const c = fact1.b;
  const b = fact2.k;
  const d = fact2.b;

  return [(d - c) / (a - b), (a * d - b * c) / (a - b)];
}

/** Расстояние */
export function distance(point1: point, point2: point) {
  return Math.sqrt(
    Math.pow(point1[0] - point2[0], 2) + Math.pow(point1[1] - point2[1], 2)
  );
}

/** Определяет, лежит ли точка С на отрезке AB */
export function pointAtCut(A: point, B: point, C: point, eps = 0.001) {
  return Math.abs(distance(C, A) + distance(C, B) - distance(A, B)) < eps;
}

export const getPixelCoord = (map: Map, geom: olGeometries) => {
  // @ts-ignore
  const coords = geom.flatCoordinates;

  const pixelCoord: [number, number][] = [];
  for (let i = 0; i < coords.length; i += 2) {
    pixelCoord.push(
      map.getPixelFromCoordinate([coords[i], coords[i + 1]]) as [number, number]
    );
  }

  return pixelCoord;
};

export const getIntersects = (
  g1: graph,
  g2: graph
): { edge1: number; edge2: number; intersect: point }[] => {
  const intersects: { edge1: number; edge2: number; intersect: point }[] = [];
  for (let i = 0; i < g1.length; i++) {
    for (let j = 0; j < g2.length; j++) {
      const inters = intersectPoint(g1[i], g2[j]);
      if (
        pointAtCut(g1[i][0], g1[i][1], inters) &&
        pointAtCut(g2[j][0], g2[j][1], inters)
      ) {
        intersects.push({ edge1: i, edge2: j, intersect: inters });
      }
    }
  }

  return intersects;
};
